package com.bitreactive.tests.tutorial.cnn.cnnspeechtest.component;

import java.util.List;
import static org.junit.Assert.*;
import no.ntnu.item.arctis.runtime.Block;

public class Component extends Block {

	public String convert(List<String> headlines) {
		if(headlines != null & headlines.size() > 0){
			return headlines.get(0);
		} else {
			return "CNN returned no headlines";
		}
	
	}

	public String errorMessage() {
		return "Error while fetching the headlines from CNN";
	}

	public void testIfFinished() {
		assertTrue(true);
	}

}
