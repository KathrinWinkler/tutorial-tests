package com.bitreactive.tests.examples.speechbuffer.speechandbuffertest.component;

import no.ntnu.item.arctis.runtime.Block;

public class Component extends Block {

	public String getFirstSentence() {
		return "All sentences are produced right after another.";
	}

	public String getSecondSentence() {
		return "With the buffer, each sentence is nicely handled after the other.";
	}

	public String getThirdSentence() {
		return "We even wait for half a second in between. Cool."; 
	}
	
}