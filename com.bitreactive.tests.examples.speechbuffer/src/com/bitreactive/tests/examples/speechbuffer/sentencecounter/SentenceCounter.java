package com.bitreactive.tests.examples.speechbuffer.sentencecounter;

import no.ntnu.item.arctis.runtime.Block;
import static org.junit.Assert.*;

public class SentenceCounter extends Block {
	int number_of_processed_sentences;

	public void countSentences() {
		number_of_processed_sentences++;
	}

	public void checkNumber() {
		assertTrue(number_of_processed_sentences == 3);
	}

	public void setCounter() {
		number_of_processed_sentences = 0;
	}

}
