package com.bitreactive.tests.tutorial.encryption.test;

import no.ntnu.item.arctis.runtime.Block;
import static org.junit.Assert.*;

public class Test extends Block {

	public java.lang.String firstString;
	public java.lang.String secondString;

	public  void compare() {
		System.out.println("first string: " + firstString);
		System.out.println("second string: " + secondString);
		assertEquals(firstString, secondString);
		
	}

	public void initialize() {
	}

}
