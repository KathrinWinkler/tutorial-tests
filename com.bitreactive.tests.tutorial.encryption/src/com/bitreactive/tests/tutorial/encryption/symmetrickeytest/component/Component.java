package com.bitreactive.tests.tutorial.encryption.symmetrickeytest.component;

import com.bitreactive.library.cryptography.Utils;

import no.ntnu.item.arctis.runtime.Block;

public class Component extends Block {

	public void setProperties() {
		setProperty("keystore", "keystore" 
              + System.getProperty("path.separator") + "my-keystore");
		setProperty("keystore-password", "changeit");
		setProperty("alias", "mykey");
		setProperty("alias-password", "changeme");
	}

	public String getPlaintext(String in) {
		System.out.println("Generated symmetric key (hexadecimal): " + in);
		String t = "Hello Arctis!";
		System.out.println("Plaintext before excryption: " + t);
		return t;
	}

	public byte[] displayCiphertext(byte[] cipher) {
		System.out.println("Ciphertext (hexadecimal): " + Utils.printBytes(cipher));
		return cipher;
	}

	public void displayPlaintext(String in) {
		System.out.println("Plaintext after decyption: " + in);
	}

}
